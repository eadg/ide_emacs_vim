;; init.el --- Emacs configuration

;; INSTALL PACKAGES
;; --------------------------------------

(require 'package)

(add-to-list 'package-archives
       '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(better-defaults
    elpy
    material-theme))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

(elpy-enable)


;; BASIC CUSTOMIZATION
;; --------------------------------------

;;stop creating backup files~

(setq make-backup-files nil) ; stop creating ~ files

(package-install 'flycheck)
(global-flycheck-mode)
(when (load "flycheck" t t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))


(setq inhibit-startup-message t) ;; hide the startup message
(load-theme 'material t) ;; load material theme
(display-line-numbers-mode t) ;; enable line numbers globally

(setq elpy-rpc-timeout nil)
(setq elpy-autodoc-delay  1)

;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(yaml-mode flymake-yamllint lsp-ui lsp-mode elpygen rust-mode csv-mode projectile use-package go-mode ace-window salt-mode skewer-mode pkg-info material-theme elpy better-defaults))
 '(safe-local-variable-values
   '((eval progn
	   (require 'grep)
	   (add-to-list
	    (make-local-variable 'grep-find-ignored-directories)
	    "site-packages"))))
 '(warning-suppress-types '((comp))))

(global-set-key (kbd "M-o") 'ace-window)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(require 'rust-mode)
(add-hook 'rust-mode-hook
          (lambda () (setq indent-tabs-mode nil)))
(setq rust-format-on-save t)

(defun goto-match-paren (arg)
  "It works but only for ( ) and run by F6"
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

(global-set-key [f6]  'goto-match-paren)


      (defun move-forward-paren (&optional arg)
       "Move forward parenthesis"
        (interactive "P")
        (if (looking-at ")") (forward-char 1))
        (while (not (looking-at ")")) (forward-char 1))) 
      
      (defun move-backward-paren (&optional arg)
       "Move backward parenthesis"
        (interactive "P")
        (if (looking-at "(") (forward-char -1))
        (while (not (looking-at "(")) (backward-char 1)))
      
      (defun move-forward-sqrParen (&optional arg)
       "Move forward square brackets"
        (interactive "P")
        (if (looking-at "]") (forward-char 1))
        (while (not (looking-at "]")) (forward-char 1)))
      
      (defun move-backward-sqrParen (&optional arg)
       "Move backward square brackets"
        (interactive "P")
        (if (looking-at "[[]") (forward-char -1))
        (while (not (looking-at "[[]")) (backward-char 1)))

      (defun move-forward-curlyParen (&optional arg)
       "Move forward curly brackets"
        (interactive "P")
        (if (looking-at "}") (forward-char 1))
        (while (not (looking-at "}")) (forward-char 1))) 
      
      (defun move-backward-curlyParen (&optional arg)
       "Move backward curly brackets"
        (interactive "P")
        (if (looking-at "{") (forward-char -1))
        (while (not (looking-at "{")) (backward-char 1)))

       (global-set-key (kbd "M-)")           (quote move-forward-paren))
       (global-set-key (kbd "M-(")           (quote move-backward-paren))
     
       (global-set-key (kbd "M-]")           (quote move-forward-sqrParen))
       (global-set-key (kbd "M-[")           (quote move-backward-sqrParen))
      
       (global-set-key (kbd "M-}")           (quote move-forward-curlyParen))
       (global-set-key (kbd "M-{")           (quote move-backward-curlyParen))

;; Company mode
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

;; Go - lsp-mode
;; Set up before-save hooks to format buffer and add/delete imports.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

;; YAML
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
(add-hook 'yaml-mode-hook 'flymake-yamllint-setup)

;;DIRED
(setq dired-listing-switches "-l")
